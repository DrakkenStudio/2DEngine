//
//  dMeshRender.swift
//  DrakkenEngine
//
//  Created by Allison Lindner on 25/08/16.
//  Copyright © 2016 Drakken Studio. All rights reserved.
//


public class dMeshRender: dComponent {
	internal var material: String?
	internal var mesh: String?
}
